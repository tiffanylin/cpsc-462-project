﻿using HangoutApp.Filters;
using HangoutApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;
using System.Text.RegularExpressions;

namespace HangoutApp.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class HomeController : Controller
    {
        private HangoutEntities db = new HangoutEntities();

        [AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }
        [AllowAnonymous]
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }
        [AllowAnonymous]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Dashboard(int? eID)
        {
            if (eID >= 0 || eID != null)
            {
                //add user to invited event
                db.AddDeleteFriends(User.Identity.Name, eID, true);
            }
            Event eve = new Event();
            User u = new User();
            System.Web.HttpContext.Current.Session["activityList"] = null;
            TempData["message"] = null;
            List<Event> events = eve.getMyEvents(u.getID(User.Identity.Name));

            return View(events);
        }
        public ActionResult GetSettings()
        {
            User u = new User();
            u = u.getInfo(User.Identity.Name);

            return View(u);
        }
        public ActionResult EditAccount()
        {
            User currentUser = new User();
            currentUser = currentUser.getInfo(User.Identity.Name);
            return View(currentUser);
        }
        [HttpPost]
        public ActionResult SubmitChanges(User u)
        {
            User currentUser = new User();

            bool correctPW = currentUser.checkPassword(User.Identity.Name, u.OldPassword);
            if (correctPW)
            {
                if (u.NewPassword != u.OldPassword)
                {

                    currentUser.submitChanges(u);
                    string identity;
                    if (u.NewEmail == null)
                    {
                        identity = User.Identity.Name;
                    }
                    else
                    {
                        identity = u.NewEmail;
                    }
                    var token = WebSecurity.GeneratePasswordResetToken(identity);
                    if (u.NewPassword == null)
                    {
                        WebSecurity.ResetPassword(token, u.OldPassword);
                    }
                    else
                    {
                        WebSecurity.ResetPassword(token, u.NewPassword);
                    }
                    WebSecurity.Logout();
                    if (String.IsNullOrEmpty(u.NewPassword))//user does not change password
                        if (String.IsNullOrEmpty(u.NewEmail))
                            WebSecurity.Login(u.Email, u.OldPassword, persistCookie: false);
                        else
                            WebSecurity.Login(u.NewEmail, u.OldPassword, persistCookie: false);
                    else//user does change password
                        if (String.IsNullOrEmpty(u.NewEmail))
                        WebSecurity.Login(u.Email, u.NewPassword, persistCookie: false);
                    else
                        WebSecurity.Login(u.NewEmail, u.NewPassword, persistCookie: false);
                    TempData["message"] = "Changes to your account have been saved";
                    return RedirectToAction("EditAccount", "Home");
                }
                else
                {
                    TempData["message"] = "Your new password cannot match with your old password";
                    return View("EditAccount");
                }
            }
            TempData["message"] = "Error with information. Please revise your information and try submitting again.";
            return View("EditAccount");

        }
        public ActionResult Cancel()
        {
            return RedirectToAction("Dashboard", "Home");
        }
        public ActionResult GetEventInvites()
        {

            return View();
        }
        public ActionResult MyEvents()
        {
            Event eve = new Event();
            User u = new User();
            List<Event> events = eve.getMyEvents(u.getID(User.Identity.Name));
            return View(events);
        }
        public ActionResult PastFriends(int eventID)
        {
            User u = new User();
            u = u.getInfo(User.Identity.Name);
            //get friends from previous event
            List<int?> friends = db.FindFriendsFromPreviousEvent(u.getID(User.Identity.Name), eventID).ToList();

            return View();
        }


        [AllowAnonymous]
        public ActionResult Screen(int eID)
        {
            Screen sc = new Screen();
            sc.eventID = eID;
            if (sc.isUser == null)
            {
                TempData["message"] = "Please check your email and try again.";
            }
            return View(sc);
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult CheckUser(Screen sc)
        {
            bool userExist = false;
            if (sc.isUser == false && String.IsNullOrEmpty(sc.UserName))
            {
                return RedirectToAction("Register", "Account");
            }
            else
            {
                userExist = WebSecurity.UserExists(sc.UserName);
            }
            if (userExist)
            {
                TempData["EventID"] = sc.eventID;
                return RedirectToAction("Login", "Account");
            }
            else
            {
                if (sc.isUser == false)
                {
                    return RedirectToAction("Register", "Account");
                }
                TempData["message"] = "Error. Please check that your email matches with the response ";
                return View("Screen", sc);
                // return RedirectToAction("Screen", sc.eventID);
            }


        }

        [HttpPost]
        public ActionResult AddActivity(DisplayEvent display)
        {
            if (display.activityModel.Name != null && display.activityModel.Location != null)
            {
                User u = new Models.User();
                display.activityModel.setEventID(display.eventModel.eventID);
                display.activityModel.addActivity(u.getID(User.Identity.Name), display.activityModel);
                TempData["message"] = "Activity successfully added";
                Event eventModel = new Models.Event();
                //eventModel = eventModel.getEventInfo();
                TempData["message"] = "Activity added";
            }
            else
            {
                TempData["message"] = "You either did not enter an Activity name or a location. Both are required.";
            }
            return RedirectToAction("Event", display.eventModel);
        }

        [HttpPost]
        public ActionResult AddActivityAtEvent(DisplayEvent displayModel)
        {
            List<Activity> activityList = (List<Activity>)System.Web.HttpContext.Current.Session["activityList"];
            if (activityList == null)
            {
                activityList = new List<Activity>();
            }
            activityList.Add(displayModel.activityModel);
            System.Web.HttpContext.Current.Session["activityList"] = activityList;
            return RedirectToAction("AddEvent", displayModel);
        }

        //[HttpPost]
        //public ActionResult AddPastFriendsAtEvent(DisplayEvent display)
        //{
        //    List<User> users = new List<User>();

        //    foreach (User u in display.userModels)
        //    {
        //        if (u.selected)
        //        {
        //            users.Add(u);

        //        }
        //    }
        //    System.Web.HttpContext.Current.Session["userList"] = users;
        //    return RedirectToAction("AddEvent", display);
        //}
        [HttpPost]
        public ActionResult AddFriends(DisplayEvent display)
        {
            Event eventModel = new Models.Event();
            //get the event model because it is not passed
            eventModel = eventModel.getEventInfo(display.eventModel.eventID);
            //this is the list of newFriends that will be passed to the function at the end
            //assuming all tests are passed 
            List<User> newFriends = new List<User>();
            if (display.emails != "" && display.emails != null) //if the user doesn't try to add anyone by email, then what is the point?
            {
                string regex = "\\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-z]{2,}\\b";
                Regex emailRegex = new Regex(regex);//the email regex
                string friends = display.emails;
                friends = friends.Replace(" ", String.Empty);
                string[] fEmails;
                fEmails = friends.Split(','); //we have all the emails sepereated out
                foreach (string f in fEmails)
                {
                    if (emailRegex.IsMatch(f)) //check the emails against the regex
                    {
                        bool dupe = false; //we need to check for dupes
                        foreach (User u in eventModel.Attendees)
                        {
                            if (u.Email == f)
                            {
                                dupe = true; //if there is a dupe, no need in continuing to search
                                break;
                            }
                        }
                        if (!dupe)
                        {
                            if (eventModel.Attendees.Count < 20)
                            {
                                User newUser = new User();
                                newUser.Email = f;
                                newFriends.Add(newUser);
                                eventModel.Attendees.Add(newUser);
                            }
                            else
                            {
                                //if 20 friends have already been added, then this is an error
                                TempData["message"] = "You have attempted to try to invite more than 20 friends.";
                                return RedirectToAction("Event", eventModel);
                            }
                        }
                        else
                        {
                            //either this user was previously added, or their email was entered twice
                            //in either case, an error
                            TempData["message"] = "You tried to invite the same friends twice. Please try again.";
                            return RedirectToAction("Event", eventModel);
                        }
                    }
                    else
                    {
                        //if the regex failed, then error
                        TempData["message"] = "One or more email addresses are invalid. Please try again";
                        return RedirectToAction("Event", eventModel);
                    }

                }
            }
            //this is for the past friends
            //note that there is no regex test
            //if the email is already in our database, then it must be valid already
            if (display.userModels != null)
            {
                foreach (User u in display.userModels)
                {
                    if (u.selected)
                    {
                        //same stuff as the dupe from before
                        bool dupe = false, isSent = true;
                        foreach (User us in eventModel.Attendees)
                        {
                            //same dupe test
                            if (us.Email == u.Email)
                            {
                                dupe = true;
                                break;
                            }
                        }
                        if (!dupe)
                        {
                            //same test to make sure that we aren't trying to have more than 20 friends at the event
                            if (eventModel.Attendees.Count < 20)
                            {
                                newFriends.Add(u);
                                eventModel.Attendees.Add(u);
                            }
                            else
                            {
                                TempData["message"] = "You have attempted to try to invite more than 20 friends.";
                                return RedirectToAction("Event", eventModel);
                            }
                        }
                        else
                        {
                            TempData["message"] = "You tried to invite the same friend twice. Please try again.";
                            return RedirectToAction("Event", eventModel);
                        }
                    }

                }
            }
            //if we ended up adding any new friends, then add them
            if (newFriends.Count > 0)
            {
                //in the event that the email cannot be added to the event, then we should display a different message
                //obviously, message sent, but how to check this?
                eventModel.addFriends(newFriends, eventModel.eventID);
                TempData["message"] = "You have added new friends to the event";

                bool isSent = false;
                foreach (User u in newFriends)
                {
                    int attendID = u.getID(u.Email);
                    if (attendID != -1)//is a registered user
                    {
                        GetUserInfo_Result rUser = db.GetUserInfo(attendID).FirstOrDefault();
                        int regUser = rUser.UserID;
                        if (rUser.Allow_Emails.HasValue && rUser.Allow_Emails.Value == true)
                        {
                            MailHelper mHelp = new MailHelper(u.Email, "", "", "");
                            mHelp.callbackURL = Url.Action("Login", "Account", new System.Web.Routing.RouteValueDictionary(new { }), "http", Request.Url.Host);
                            isSent = mHelp.inviteFriends(mHelp);
                            if (!isSent)
                            {
                                break;
                            }

                        }
                    }
                    else //is not a registered user
                    {
                        MailHelper mHelp = new MailHelper(u.Email, "", "", "");
                        mHelp.callbackURL = Url.Action("Screen", "Home", new System.Web.Routing.RouteValueDictionary(new { eID = eventModel.eventID }), "http", Request.Url.Host);
                        isSent = mHelp.inviteFriends(mHelp);
                        if (!isSent)
                        {
                            break;
                        }
                    }
                }
            }

            return RedirectToAction("Event", eventModel);

        }
        [HttpPost]
        public ActionResult FinalizeDetails(DisplayEvent displayModel)
        {
            string button = Request.Params["finalizeBtn"];
            int eventID = displayModel.eventModel.eventID;
            Event newEvent = new Event();
            newEvent = newEvent.getEventInfo(eventID);
            //pastFriends = newEvent.getPastFriends(User.Identity.Name, eventID);

            User us = new Models.User();

            int uID = us.getID(User.Identity.Name);
            if (newEvent.Attendees == null)
            {
                newEvent.Attendees.Add(us);
            }
            displayModel.eventModel.Attendees = newEvent.Attendees;
            //populate to see if checkbox will be checked or not

            //get all the suggested times from the user
            if (button == "Finalize Event")
            {
                int result = db.FinalizeEvent(displayModel.eventModel.adminID, displayModel.eventModel.eventID, true);
                bool isSent = false;
                if (result == -1)//if sp unable to finalize event
                {
                    TempData["message"] = "There was an issue with finalizing the event. Please check the details of your event and try finalizing again. If issue persists, please contact us.";
                    return View("Event", displayModel);
                }
                else
                {
                    TempData["message"] = "Event has been finalized. Attendees have been notified of finalized details";
                    //send email to all opt-in-email attendeees
                    User cUser = new User();
                    cUser = us.getInfo(User.Identity.Name);

                    string mailBody = "Your event: " + displayModel.eventModel.Name + " has been finalized. It is taking place at " +
                        displayModel.eventModel.Location + " starting at " + displayModel.eventModel.Time + " on " + displayModel.eventModel.Date
                        + ". If there are any further questions about this event. Please contact the admin of your event at " + cUser.Email;
                    foreach (User attendee in displayModel.eventModel.Attendees)
                    {
                        //check to see if attendee's email is registered in db
                        int attenID = attendee.getID(attendee.Email);
                        if (attenID != -1)//is a registered user
                        {
                            GetUserInfo_Result rUser = db.GetUserInfo(attenID).FirstOrDefault();
                            int regUser = rUser.UserID;
                            //only send email to user if user has allowed emails
                            if (rUser.Allow_Emails.HasValue && rUser.Allow_Emails.Value == true)
                            {
                                MailHelper mHelp = new MailHelper(attendee.Email, "", "", mailBody);
                                isSent = mHelp.sendFinalDetails(mHelp);
                                if (!isSent)
                                {
                                    break;
                                }
                            }
                        }
                        else//is not a registered user
                        {
                            MailHelper mHelp = new MailHelper(attendee.Email, "", "", "");
                            mHelp.callbackURL = Url.Action("Screen", "Home", new System.Web.Routing.RouteValueDictionary(new { eID = eventID }), "http", Request.Url.Host);
                            isSent = mHelp.sendFinalDetails(mHelp);
                            if (!isSent)
                            {
                                break;
                            }
                        }
                    }
                }
                //if (isSent)
                TempData["message"] = "Event " + displayModel.eventModel.Name + " has been finalized.";
                //else
                //    TempData["message"] = "Event finalized. No emails were sent";

            }
            else if (button == "finalizeTime")
            {//finalize time
                if (displayModel.eventModel.Time != "" && displayModel.eventModel.Date != "")
                {
                    if (DateTime.Now >= DateTime.Parse(displayModel.eventModel.suggestedDate + " " + displayModel.eventModel.suggestedTime))
                    {
                        displayModel.eventModel.finalizeTime(displayModel.eventModel);
                        TempData["message"] = "Event time finalized";
                    }
                    else
                    {
                        TempData["message"] = "You must select a time in the future!";
                    }
                }
                else
                {
                    TempData["message"] = "You have to enter both a date and time!";
                }


            }
            else if (button == "finalizeActivity")
            {//finalize activity
                if (displayModel.eventModel.Activities != null)
                {
                    foreach (Activity a in displayModel.eventModel.Activities)
                    {
                        if (a.finalize)
                        {
                            a.finalizeActivity(a, eventID);
                        }
                    }
                    TempData["message"] = "Activities have been finalized.";
                }
            }
            else if (button == "CancelEvent")
            {//cancel event
                displayModel.eventModel.cancelEvent(displayModel.eventModel);
                TempData["message"] = "Event has been canceled.";
                return RedirectToAction("Dashboard", "Home");
            }
            return RedirectToAction("Event", displayModel.eventModel);

        }
        public ActionResult AddEvent(DisplayEvent displayModel)
        {
            if (displayModel.eventModel == null)
            {
                List<User> pastFriends = new List<User>();
                Event nEvent = new Event();
                nEvent.Activities = new List<Activity>();
                nEvent.Attendees = new List<User>();
                nEvent.Attendees.Add(new User());
                pastFriends = nEvent.getPastFriends(User.Identity.Name, 0);
                List<User> users = (List<User>)System.Web.HttpContext.Current.Session["userList"];
                System.Web.HttpContext.Current.Session["activityList"] = null;

                displayModel.userModels = pastFriends;
                displayModel.eventModel = nEvent;
                displayModel.activityModel = new Activity();
            }
            if(displayModel.userModels == null)
            {
                List<User> pastFriends = new List<User>();
                pastFriends = displayModel.eventModel.getPastFriends(User.Identity.Name, 0);
                displayModel.userModels = pastFriends;
            }
            return View(displayModel);
        }


        [HttpPost]
        public ActionResult AddNewEvent(DisplayEvent displayModel)
        {
            Event newEvent = new Event();
            User u = new User();
            string regex = "\\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-z]{2,}\\b";
            Regex emailRegex = new Regex(regex);//the email regex
            if (displayModel.eventModel.Activities == null)
            {
                displayModel.eventModel.Activities = new List<Activity>();
            }
            string button = Request.Params["submitBtn"];
            if (button == "AddActivityAtEvent")
            {
                //get the activity saved and put into eventModel.activities

                displayModel.eventModel.Activities.Add(displayModel.activityModel);
                //refresh the activity model in case the user wants to suggest another activity
                displayModel.activityModel = new Activity();
                if (displayModel.eventModel.Attendees == null)
                {
                    displayModel.eventModel.Attendees.Add(new User());
                }
                TempData["AddEventmessage"] = "New activity added";
                return View("AddEvent", displayModel);
            }
            else if (button == "Submit")
            {
                //create the event ID for new event
                bool pass = false;
                if (displayModel.eventModel.Date == null)
                {
                    pass = true;
                }
                else
                {
                    if(displayModel.eventModel.Time == null)
                    {
                        if(DateTime.Now >= DateTime.Parse(displayModel.eventModel.Date))
                        {
                            pass = false;
                        }
                        else
                        {
                            pass = true;
                        }
                    }
                    else
                    {
                        if(DateTime.Now >= DateTime.Parse(displayModel.eventModel.Date + " " + displayModel.eventModel.Time))
                        {
                            pass = false;
                        }
                        else
                        {
                            pass = true;
                        }
                    }
                }
                int eventID;

                if (pass)
                {
                    eventID = newEvent.addEvents(u.getID(User.Identity.Name), displayModel.eventModel);
                    string friends = displayModel.eventModel.Attendees[0].Email;
                    displayModel.eventModel.Attendees.RemoveAt(0);

                    string[] fEmails;
                    if (friends != null)
                    {
                        friends = friends.Replace(" ", String.Empty);
                        fEmails = friends.Split(',');


                        foreach (string f in fEmails)
                        {
                            f.Replace(" ", String.Empty);

                            if (emailRegex.IsMatch(f))
                            {
                                bool dupe = false; //we need to check for dupes
                                foreach (User us in displayModel.eventModel.Attendees)
                                {
                                    if (us.Email == f)
                                    {
                                        dupe = true; //if there is a dupe, no need in continuing to search
                                        break;
                                    }
                                }
                                if (!dupe)
                                {
                                    if (displayModel.eventModel.Attendees.Count < 20)
                                    {
                                        User user = new User();
                                        user.Email = f;
                                        displayModel.eventModel.Attendees.Add(user);
                                    }
                                    else
                                    {
                                        //if 20 friends have already been added, then this is an error
                                        TempData["AddEventmessage"] = "You have attempted to try to invite more than 20 friends.";
                                        return RedirectToAction("AddEvent", displayModel);
                                    }
                                }
                                else
                                {
                                    TempData["AddEventmessage"] = "You tried to invite the same friend twice. Please try again.";
                                    return RedirectToAction("AddEvent", displayModel);
                                }

                            }
                            else
                            {
                                TempData["AddEventmessage"] = "One or more email addresses are invalid. Please try again";
                                return RedirectToAction("AddEvent", displayModel);
                            }
                        }
                    }
                }
                else
                {
                    TempData["AddEventmessage"] = "If you decide to enter a date and/or a time, it must be in the future";
                    List<User> pastFriends = new List<User>();
                    pastFriends = displayModel.eventModel.getPastFriends(User.Identity.Name, 0);
                    displayModel.userModels = pastFriends;
                    return View("AddEvent", displayModel);
                }
                List<User> users = (List<User>)System.Web.HttpContext.Current.Session["userList"];

                if (users != null)
                {
                    foreach (User us in users)
                    {
                        //same stuff as the dupe from before
                        bool dupe = false;
                        foreach (User user in displayModel.eventModel.Attendees)
                        {
                            //same dupe test
                            if (user.Email == us.Email)
                            {
                                dupe = true;
                                break;
                            }
                        }
                        if (!dupe)
                        {
                            //same test to make sure that we aren't trying to have more than 20 friends at the event
                            if (displayModel.eventModel.Attendees.Count < 20)
                            {
                                displayModel.eventModel.Attendees.Add(us);
                            }
                            else
                            {
                                TempData["AddEventmessage"] = "You have attempted to try to invite more than 20 friends.";
                                return RedirectToAction("Event", displayModel.eventModel);
                            }
                        }
                        else
                        {
                            TempData["AddEventmessage"] = "You tried to invite the same friend twice. Please try again.";
                            return RedirectToAction("Event", displayModel.eventModel);
                        }

                    }
                }
                bool isSent = true;
                foreach (var a in displayModel.eventModel.Attendees)
                {
                    //check to see if attendee's email is registered in db
                    int attenID = a.getID(a.Email);

                    if (attenID != -1)//is a registered user
                    {
                        GetUserInfo_Result rUser = db.GetUserInfo(attenID).FirstOrDefault();
                        int regUser = rUser.UserID;
                        //only send email to user if user has allowed emails
                        if (rUser.Allow_Emails.HasValue && rUser.Allow_Emails.Value == true)
                        {
                            MailHelper mHelp = new MailHelper(a.Email, "", "", "");
                            mHelp.callbackURL = Url.Action("Login", "Account", new System.Web.Routing.RouteValueDictionary(new { }), "http", Request.Url.Host);
                            isSent = mHelp.inviteFriends(mHelp);
                            if (!isSent)
                            {
                                break;
                            }
                        }
                    }
                    else//is not a registered user
                    {
                        MailHelper mHelp = new MailHelper(a.Email, "", "", "");
                        mHelp.callbackURL = Url.Action("Screen", "Home", new System.Web.Routing.RouteValueDictionary(new { eID = eventID }), "http", Request.Url.Host);
                        isSent = mHelp.inviteFriends(mHelp);
                        if (!isSent)
                        {
                            break;
                        }
                    }
                }
                if (isSent)
                {
                    TempData["message"] = "Event succesfully added and invites successfully sent";
                    //return View("AddEvent", displayModel);
                    return RedirectToAction("Dashboard");
                }
                TempData["AddEventmessage"] = "Event successfully added but Error with Email. Please revise invited friends' email addresses";
                return View("AddEvent", displayModel);
            }
            else if (button == "AddPastFriendsAtEvent")
            {
                List<User> users = new List<User>();

                foreach (User us in displayModel.userModels)
                {
                    if (us.selected)
                    {
                        users.Add(us);
                    }
                }
                System.Web.HttpContext.Current.Session["userList"] = users;
                TempData["AddEventmessage"] = "Friends selected from previous events added to current event";
                return View("AddEvent", displayModel);
            }
            else if (button == "Cancel")
            {
                return RedirectToAction("AddEvent");
            }
            else
            {
                return RedirectToAction("AddEvent", displayModel);
            }

        }

        public ActionResult Event(int eventID)
        {
            Activity activityModel = new Activity();
            List<User> pastFriends = new List<User>();
            Event eventModel = new Event();
            Event newEvent = eventModel.getEventInfo(eventID);
            pastFriends = eventModel.getPastFriends(User.Identity.Name, eventModel.getEventID());

            User us = new Models.User();
            int uID = us.getID(User.Identity.Name);
            if (newEvent.Attendees == null)
            {
                newEvent.Attendees.Add(us);
            }
            DisplayEvent displayModel = new DisplayEvent();
            displayModel.eventModel = newEvent;
            displayModel.activityModel = activityModel;
            //populate to see if checkbox will be checked or not
            for (int i = 0; i < newEvent.Activities.Count; i++)
            {
                var am = newEvent.Activities[i];
                bool isVoted = false;
                if (db.CheckActivityVoteByUser(uID, am.activityID).FirstOrDefault().HasValue)
                    isVoted = db.CheckActivityVoteByUser(uID, am.activityID).FirstOrDefault().Value;
                displayModel.eventModel.Activities[i].selected = isVoted;
            }
            displayModel.userModels = pastFriends;
            //get all the suggested times from the user
            //go here
            UserTimes t = new UserTimes();
            displayModel.suggestedTimes = t.GetAllSuggestedTimes(displayModel.eventModel.eventID);

            System.Web.HttpContext.Current.Session["eventID"] = eventModel.getEventID();
            ViewBag.Message = "Event page";
            return View(displayModel);

        }

        [HttpPost]
        public ActionResult DeleteActivity(DisplayEvent display)
        {
            if (display.eventModel.Activities != null)
            {
                bool deleted = false;
                foreach (Activity act in display.eventModel.Activities)
                {
                    if (act.selected)
                    {
                        deleted = true;
                        act.deleteActivity(act, display.eventModel.eventID);

                    }
                }
                if (deleted)
                {
                    TempData["message"] = "Activities successfully deleted.";
                }
                else
                {
                    TempData["message"] = "You did not select any activities to be deleted";
                }
            }
            else
            {
                TempData["message"] = "There are no activities to delete. Add some!";
            }

            Event eventModel = new Models.Event();
            eventModel = eventModel.getEventInfo(display.eventModel.eventID);
            return RedirectToAction("Event", eventModel);
        }

        [HttpPost]
        public ActionResult DeleteUser(DisplayEvent display)
        {

            if (display.eventModel.Attendees != null)
            {
                bool deleted = false;
                foreach (User u in display.eventModel.Attendees)
                {
                    if (u.selected)
                    {
                        display.eventModel.deleteUser(u, display.eventModel.eventID, display.eventModel.adminID);
                        deleted = true;
                    }
                }
                if (deleted)
                {
                    TempData["message"] = "You have successfully removed those users from your event.";
                }
                else
                {
                    TempData["message"] = "You did not select anyone to be deleted!";
                }

            }
            else
            {
                TempData["message"] = "You did not select anyone to be deleted!";
            }
            Event eventModel = new Models.Event();
            eventModel = eventModel.getEventInfo(display.eventModel.eventID);

            return RedirectToAction("Event", eventModel);
        }

        [HttpPost]
        public ActionResult VoteForTime(DisplayEvent displayModel)
        {
            User u = new User();
            int uID = u.getID(User.Identity.Name);
            int eID = displayModel.eventModel.eventID;
            if (displayModel.eventModel.suggestedTime != "" && displayModel.eventModel.suggestedTime != null && displayModel.eventModel.suggestedDate != "" && displayModel.eventModel.suggestedDate != null)
            {
                if (DateTime.Now < DateTime.Parse(displayModel.eventModel.suggestedDate + " " + displayModel.eventModel.suggestedTime))
                {
                    db.VoteForTime(uID, eID, displayModel.eventModel.suggestedTime, displayModel.eventModel.suggestedDate);
                    TempData["message"] = "Your vote for event time has been submitted";
                }
                else
                {
                    TempData["message"] = "You must enter a time in the future.";
                }
            }
            else
            {
                TempData["message"] = "You either did not enter a date or a time. Both are required.";
            }

            return RedirectToAction("Event", displayModel.eventModel);
        }

        [HttpPost]
        public ActionResult VoteActivity(DisplayEvent displayModel)
        {
            User u = new User();
            int uID = u.getID(User.Identity.Name);
            int eID = displayModel.eventModel.eventID;

            if (displayModel.eventModel.Activities != null)
            {
                bool voted = false;
                foreach (var a in displayModel.eventModel.Activities)
                {
                    if (a.selected)//if true then add vote for activity
                    {
                        db.VoteForActivity(a.activityID, uID);
                        voted = true;
                    }
                }
                if (voted)
                {
                    TempData["message"] = "Your vote has been updated.";
                }
                else
                {
                    TempData["message"] = "You did not select any activities!";
                }
            }
            else
            {
                TempData["message"] = "There are no events!";
            }
            return RedirectToAction("Event", displayModel.eventModel);
        }

    }


}


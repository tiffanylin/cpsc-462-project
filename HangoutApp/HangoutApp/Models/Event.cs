﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.Web;

namespace HangoutApp.Models
{
    public class Event
    {
        public int eventID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Time { get; set; }
        public string Location { get; set; }
        public string Date { get; set; }
        public int adminID { get; set; }
        public bool finalized { get; set; }
        public List<User> Attendees { get; set; }
        public List<Activity> Activities { get; set; }
        public bool activityApproved { get; set; }
        public string suggestedTime { get; set; }
        public string suggestedDate { get; set; }
        public bool timePassed { get; set; }
        public Event()
        {

        }

        public int getEventID()
        {
            return eventID;
        }
        public Event getEventInfo(int eID)
        {
            HangoutEntities db = new HangoutEntities();
            GetEventInfo_Result result = db.GetEventInfo(eID).FirstOrDefault();
            List<GetActivityInfo_Result> activityResultList = db.GetActivityInfo(eID).ToList();
            List<GetAttendeesOfEvent_Result> attendees = db.GetAttendeesOfEvent(eID).ToList();
            Event currentEvent = new Event();
            currentEvent.eventID = result.EventID;
            currentEvent.Name = result.EventName;
            currentEvent.Description = result.EventDescription;
            currentEvent.Time = result.EventTime;
            currentEvent.Location = result.Location;
            currentEvent.Date = result.Date;
            currentEvent.adminID = result.AdminID;
            currentEvent.finalized = result.Approved;
            currentEvent.activityApproved = false;
            if(currentEvent.Date == null)
            {
                currentEvent.timePassed = false;
            }
            else
            {
                if(currentEvent.Time == null)
                {
                    if(DateTime.Now >= DateTime.Parse(currentEvent.Date))
                    {
                        currentEvent.timePassed = true;
                    }
                    else
                    {
                        currentEvent.timePassed = false;
                    }
                }
                else
                {
                    if(DateTime.Now >= DateTime.Parse(currentEvent.Date + " " + currentEvent.Time))
                    {
                        currentEvent.timePassed = true;
                    }
                    else
                    {
                        currentEvent.timePassed = false;
                    }

                }
            }
            currentEvent.Activities = new List<Activity>();
            currentEvent.Attendees = new List<User>();
            foreach (var a in activityResultList)
            {
                Activity act = new Activity();
                act.Name = a.ActivityName;
                act.userID = a.UserID;
                act.activityID = a.ActivityID;
                act.Location = a.Location;
                act.selected = false;
                act.Votes = act.getNumVotes(act.activityID).ToString();
                act.finalize = Convert.ToBoolean(a.Approved);
                if(act.finalize)
                {
                    currentEvent.activityApproved = true;
                }
                act.Description = a.ActivityDescription;
                currentEvent.Activities.Add(act);
            }
            foreach (var u in attendees)
            {
                User newUser = new User();
                newUser.FirstName = u.firstName;
                newUser.LastName = u.lastName;
                newUser.Email = u.Email;
                newUser.selected = false;
                currentEvent.Attendees.Add(newUser);
            }
            return currentEvent;
        }
        //add new event to db
        public int addEvents(int userID, Event newEvent)
        {
            HangoutEntities db = new HangoutEntities();
            //use stored procedure to add event to db
            //int result=db.EventMasterProc(friends, null, false, false, newEvent.Name, newEvent.Description,
            // newEvent.Time, newEvent.Location, newEvent.Date, false,userID, false, true,
            // null,null,null,null,null,null,null,null,null,false, false);
            ObjectParameter eID = new ObjectParameter("EventID", typeof(global::System.Int32));
            eID.Value = newEvent.eventID;
            //eID holds value of the event just created
            int result = db.CreateEditEventPage(newEvent.Name, newEvent.Description, newEvent.Time, newEvent.Location,
                newEvent.Date, false, userID, eID, false);
            if (newEvent.Activities != null)
            {
                foreach (Activity a in newEvent.Activities)
                {
                    db.AddEditActivity(null, a.Description, Convert.ToInt32(eID.Value), a.Location, false, false, 0, userID, a.Name, false);
                }
            }
            foreach (User u in newEvent.Attendees)
            {
                db.AddDeleteFriends(u.Email, Convert.ToInt32(eID.Value), true);
            }
            return Convert.ToInt32(eID.Value);
        }

        public void deleteUser(User u, int eventID, int adminID)
        {
            HangoutEntities db = new HangoutEntities();
            db.AddDeleteFriends(u.Email, eventID, false);
            
            //now, we need to get all of the 
            //List<int?> activityIDs = db.GetAllActivitiesUserSuggestedForEvent(u.getID(u.Email), eventID).ToList();
            //then update them to have the Event Admin as their user
            /*for(int i = 0; i < activityIDs.Count; i++)
            {
                db.UpdateAdminOfActivity(adminID, eventID, activityIDs[i]);
            }*/
        }

        public List<Event> getMyEvents(int userID)
        {
            HangoutEntities db = new HangoutEntities();
            List<GetUserEvents_Result> events = db.GetUserEvents(userID).ToList();
            List<Event> allEvents = new List<Event>();
            foreach (var eve in events)
            {
                Event newEvent = new Event();
                newEvent.eventID = eve.EventID;
                newEvent.Name = eve.EventName;
                newEvent.Description = eve.EventDescription;
                newEvent.Date = eve.Date;
                newEvent.Location = eve.Location;
                newEvent.Time = eve.EventTime;
                newEvent.eventID = eve.EventID;
                allEvents.Add(newEvent);
            }

            return allEvents;
        }

        public void cancelEvent(Event eventModel)
        {
            HangoutEntities db = new HangoutEntities();
            ObjectParameter eID = new ObjectParameter("EventID", eventModel.eventID);
            int result = db.CreateEditEventPage(eventModel.Name, eventModel.Description, eventModel.Time, eventModel.Location,
                eventModel.Date, true, eventModel.adminID, eID, true);
        }

        public void finalizeTime(Event eventModel)
        {
            HangoutEntities db = new HangoutEntities();
            ObjectParameter eID = new ObjectParameter("EventID", eventModel.eventID);
            int result = db.CreateEditEventPage(eventModel.Name, eventModel.Description, eventModel.Time, eventModel.Location, eventModel.Date, false, eventModel.adminID, eID, true);
        }

        public void addFriends(List<User> newFriends, int eventID)
        {
            HangoutEntities db = new HangoutEntities();
            foreach (User u in newFriends)
            {
                db.AddDeleteFriends(u.Email, eventID, true);
            }
        }

        public List<User> getPastFriends(string userEmail, int eventID)
        {
            HangoutEntities db = new HangoutEntities();
            User u2 = new User();
            List<int?> events = db.FindPreviousEvent(eventID, u2.getID(userEmail)).ToList();
            List<GetAttendeesOfEvent_Result> friends = new List<GetAttendeesOfEvent_Result>();
            List<User> friendsU = new List<User>();

            foreach (var e in events)
            {
                friends = db.GetAttendeesOfEvent(e).ToList();
                foreach (var u in friends)
                {
                    if (userEmail != u.Email)
                    {
                        User us = new User();
                        us.Email = u.Email;
                        us.FirstName = u.firstName;
                        us.LastName = u.lastName;
                        us.selected = false;
                        bool noDupes = true;
                        foreach (var i in friendsU)
                        {
                            if (i.Email == us.Email)
                            {
                                noDupes = false;
                            }
                        }
                        if (noDupes)
                        {
                            friendsU.Add(us);
                        }
                    }
                }

            }

            return friendsU;
        }

        public bool UserHasSuggestedTime(int userID, List<UserTimes> times)
        {
            if (times != null)
            {
                foreach (UserTimes t in times)
                {
                    if (t.userID == userID)
                    {
                        return true;
                    }
                }
            }
            return false;
        }



    }
}
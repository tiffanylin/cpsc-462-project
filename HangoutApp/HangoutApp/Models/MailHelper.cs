﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Policy;
using System.Web;

namespace HangoutApp.Models
{
    public class MailHelper
    {
        private const int connTimeout = 180000;
        private readonly string host;
        private readonly int port;
        private readonly string authUser;
        private readonly string authPass;
        private readonly bool sslEnabled;

        public string callbackURL { get; set; }
        public string Sender { get; set; }
        public string Recipient { get; set; }
        public string RecipientCC { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string AttachmentFile { get; set; }
        public MailHelper(string recip, string cc, string sub, string body)
        {
            //MailServer - Represents the SMTP Server
            host = ConfigurationManager.AppSettings["Mail_Server"];
            //Port- Represents the port number
            port = int.Parse(ConfigurationManager.AppSettings["Port"]);
            Sender = ConfigurationManager.AppSettings["EmailFromAddress"];
            authUser = ConfigurationManager.AppSettings["MailAuthUser"];
            authPass = ConfigurationManager.AppSettings["MailAuthPass"];
            sslEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSL"]);
            Recipient = recip;
            RecipientCC = cc;
            Subject = sub;
            Body = body;
        }
        //sends email to user when they first create account (confirms account)
        public bool Send(MailHelper m)
        {

            try
            {
                SmtpClient client = new SmtpClient(m.host);
                client.Credentials = new NetworkCredential(m.authUser, m.authPass);
                MailAddress fromEm = new MailAddress(m.authUser, "HangoutApp Info");
                MailAddress toEm = new MailAddress(m.Recipient, "Recipient");
                MailMessage mailMsg = new MailMessage(fromEm, toEm);
                mailMsg.Body = "Thank you for creating a new account with us";
                mailMsg.Subject = "HangoutApp - New Account Created";
                client.EnableSsl = true;
                client.Send(mailMsg);

                return true;
            }

            catch (Exception ex)
            {
                return false;
            }
            
        }
        //sends email to friends of user invited to event
        
        public bool inviteFriends(MailHelper m)
        {
            
            try
            {
                SmtpClient client = new SmtpClient(m.host);
                client.Credentials = new NetworkCredential(m.authUser, m.authPass);
                MailAddress fromEm = new MailAddress(m.authUser, "HangoutApp Info");
                MailAddress toEm = new MailAddress(m.Recipient, "Recipient");
                MailMessage mailMsg = new MailMessage(fromEm, toEm);
                //change link later
                mailMsg.Body = "Please use this link to go to event: " +m.callbackURL;
                mailMsg.IsBodyHtml = true;
                mailMsg.Subject = "HangoutApp - You have been invited to an event";
                client.EnableSsl = true;
                client.Send(mailMsg);

                return true;
            }

            catch (Exception ex)
            {
                return false;
            }
        }
        public bool sendFinalDetails(MailHelper m)
        {
            if (m.Body == "")
            {
                m.Body = "An event you have been invited to is finalized. To see the details of this event, please log in to your registered account or register free for an account. Please use the link for further details: " + m.callbackURL;
            }
            try
            {
                SmtpClient client = new SmtpClient(m.host);
                client.Credentials = new NetworkCredential(m.authUser, m.authPass);
                MailAddress fromEm = new MailAddress(m.authUser, "HangoutApp Info");
                MailAddress toEm = new MailAddress(m.Recipient, "Recipient");
                MailMessage mailMsg = new MailMessage(fromEm, toEm);
                //change link later
                mailMsg.Body = m.Body;
                mailMsg.IsBodyHtml = true;
                mailMsg.Subject = "HangoutApp - An Event You are Attending Has Been Finalized";
                client.EnableSsl = true;
                client.Send(mailMsg);

                return true;
            }

            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HangoutApp.Models
{
    public class Account
    {
        public User currentUser { get; set; }
        public List<Event> attendingEvents { get; set; }
        public List<Event> invitedEvents { get; set; }
        public Account() { }
           
    }
}
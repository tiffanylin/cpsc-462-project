﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HangoutApp.Models
{
    public class UserTimes
    {
        public string suggestedTime { get; set; }
        public string suggestedDate { get; set; }
        public int userID { get; set; }
        public int popularity { get; set; }

        public UserTimes()
        {

        }

        public List<UserTimes> GetAllSuggestedTimes(int eventID)
        {
            HangoutEntities db = new HangoutEntities();
            List<GetAllAttendeesEventTime_Result> suggestTimes = db.GetAllAttendeesEventTime(eventID).ToList();
            List<UserTimes> times = new List<UserTimes>();
            foreach(var t in suggestTimes)
            {
                UserTimes suggest = new UserTimes();
                suggest.suggestedTime = t.Time;
                suggest.suggestedDate = t.Date;
                suggest.userID = t.UserID;
                suggest.popularity = 1;
                times.Add(suggest);
            }
            return times;
        }

        //public List<UserTimes> GetPopularTimes(List<UserTimes> suggestedTimes)
        //{
        //    List<UserTimes> popularTimes = new List<UserTimes>();

        //}

    }
}
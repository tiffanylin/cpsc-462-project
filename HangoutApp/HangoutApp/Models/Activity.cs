﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HangoutApp.Models
{
    public class Activity
    {
        public int activityID { get; set; }
        public int eventID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int userID { get; set; }
        public string Location { get; set; }
        public string Votes { get; set; }
        public bool selected { get; set; }
        public bool finalize { get; set; }
        public bool Approved { get; set; }
        public Activity()
        {

        }

        public void addActivity(int userID, Activity newActivity)
        {
            HangoutEntities db = new HangoutEntities();
            db.AddEditActivity(0, newActivity.Description, newActivity.eventID, newActivity.Location,
                false, false, 0, userID, newActivity.Name, false);
        }

        public void finalizeActivity(Activity newActivity, int eventID)
        {
            HangoutEntities db = new HangoutEntities();
            int votes = Convert.ToInt32(newActivity.Votes);
            db.AddEditActivity(newActivity.activityID, newActivity.Description, eventID, newActivity.Location, false, true, votes, newActivity.userID, newActivity.Name, true);
        }

        public void setEventID(int eID)
        {
            eventID = eID;
        }

        public int getActivityID()
        {
            return activityID;
        }

        public List<Activity> getActivityInfo(int eventID)
        {
            HangoutEntities db = new HangoutEntities();
            List<GetActivityInfo_Result> activities = db.GetActivityInfo(eventID).ToList();
            List<Activity> allActivities = new List<Activity>();
            foreach (var a in activities)
            {
                Activity newActivity = new Activity();
                newActivity.activityID = a.ActivityID; //maybe not needed?
                newActivity.Name = a.ActivityName;
                newActivity.Description = a.ActivityDescription;
                newActivity.Location = a.Location;
                newActivity.Votes = newActivity.getNumVotes(newActivity.activityID).ToString();
                newActivity.selected = false;
                newActivity.Approved = Convert.ToBoolean(a.Approved);
                allActivities.Add(newActivity);
            }

            return allActivities;
        }

        public int getNumVotes(int activityid)
        {
            HangoutEntities db = new HangoutEntities();
            GetNumVotesForActivity_Result result = db.GetNumVotesForActivity(activityID).FirstOrDefault();
            return Convert.ToInt32(result.Column1);
        }

        public void deleteActivity(Activity act, int eventID)
        {
            HangoutEntities db = new HangoutEntities();

            db.AddEditActivity(act.activityID, act.Description, eventID, act.Location, true, false, 0, act.userID, act.Name, true);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;
using System.Linq;
namespace HangoutApp.Models
{
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
    }

    public class Screen{
         public bool isUser {get; set;}
         public string UserName {get; set;}
         public int eventID { get; set; }
         public Screen() { }
    }
    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "Email")]
        [RegularExpression("\\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-z]{2,}\\b", ErrorMessage = "Email address is invalid")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        public int holdEvent { get; set; }

        public LoginModel() { }
    }
    public class User
    {
        private int ID { get; set; }
        [RegularExpression("\\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-z]{2,}\\b", ErrorMessage = "Email address is invalid")]
        public string Email { get; set; }
        [RegularExpression("\\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-z]{2,}\\b", ErrorMessage = "Email address is invalid")]
        public string NewEmail { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        public string NewPassword { get; set; }
        public string OldPassword { get; set; }
        public bool selected { get; set; }
        public bool AllowEmail { get; set; }
        public User() { }
        public User getInfo(string userName)
        {
            User u = new User();
            int id = WebSecurity.GetUserId(userName);
            HangoutEntities db = new HangoutEntities();
            GetUserInfo_Result info = db.GetUserInfo(id).FirstOrDefault();
            u.ID = info.UserID;
            u.Email = info.Email;
            u.FirstName = info.firstName;
            u.LastName = info.lastName;
            return u;
        }
        public int getID(string userName)
        {
            User u = new User();
            int id = WebSecurity.GetUserId(userName);
            return id;
        }
        public bool checkPassword(string userName, string oldPW)
        {
            User u = new User();
            int id = WebSecurity.GetUserId(userName);
            HangoutEntities db = new HangoutEntities();
            GetUserInfo_Result info = db.GetUserInfo(id).FirstOrDefault();
            if (info.password == oldPW)
            {
                return true;
            }
            return false;
        }

        public void submitChanges(User u)
        {
            HangoutEntities db = new HangoutEntities();
            //call stored procedure that edits user info
            int id = WebSecurity.GetUserId(u.Email);
            User modifiedUser = new User();
            modifiedUser = modifiedUser.getInfo(u.Email);
            //in case the user does not submit a new password value;
            if (String.IsNullOrEmpty(u.NewPassword))
            {
                u.NewPassword = u.OldPassword;
            }
            if (String.IsNullOrEmpty(u.FirstName))
            {
                u.FirstName = modifiedUser.FirstName;
            }
            if (String.IsNullOrEmpty(u.LastName))
            {
                u.LastName = modifiedUser.LastName;
            }
            //checks if user submits a new email
            if (String.IsNullOrEmpty(u.NewEmail))
            {
                db.CreateEditAccount(u.Email, u.NewPassword, u.FirstName, u.LastName, u.AllowEmail, id, true);
            }
            else
            {
                db.CreateEditAccount(u.NewEmail, u.NewPassword, u.FirstName, u.LastName, u.AllowEmail, id, true);
            }

        }

    }
    public class RegisterModel
    {
        protected int UserID { get; set; }

        [Required]
        [RegularExpression("\\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-z]{2,}\\b", ErrorMessage = "Email address is invalid")]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [RegularExpression("(?=.+[0-9])[a-zA-Z0-9]{6,}", ErrorMessage = "Password is invalid. Passwords must contain at least 1 number and be six characters long.")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string password { get; set; }

        //[DataType(DataType.Password)]
        //[Display(Name = "Confirm password")]
        //[Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        //public string ConfirmPassword { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public bool Allow_Emails { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace HangoutApp.Models
{
    public class DisplayEvent
    {
        public Event eventModel { get; set; }
        public Activity activityModel { get; set; }
        public List<User> userModels { get; set; }
        public string emails { get; set; }
        public List<UserTimes> suggestedTimes { get; set; }
        public List<UserTimes> popularTimes { get; set; }
        public DisplayEvent()
        {

        }
        
    }

}
